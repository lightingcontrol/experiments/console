package io.gitlab.lightingcontrol.console;

import io.gitlab.lightingcontrol.console.configuration.Channel;
import io.gitlab.lightingcontrol.console.configuration.ChannelType;
import io.gitlab.lightingcontrol.console.configuration.Configuration;
import io.gitlab.lightingcontrol.console.panel.ChannelPanel;
import io.gitlab.lightingcontrol.console.util.LightingControlShowFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Vector;

import static java.util.Objects.requireNonNull;

public class LightingControlConsole extends JFrame {
    private static final Logger LOGGER = LoggerFactory.getLogger(LightingControlConsole.class);

    public static final String TITLE = "LightingControl";

    private LightingControlConsole() {
        super();

        JMenuBar bar = buildMenuBar(this);
        setJMenuBar(bar);

        ChannelPanel channels = new ChannelPanel();

        setIconImage(new ImageIcon(requireNonNull(getClass().getResource("/lighting-control.png"))).getImage());

        Configuration configuration = new Configuration(new Vector<>());
        configuration.channels().add(new Channel("Red", ChannelType.PERCENTAGE));
        configuration.channels().add(new Channel("Green", ChannelType.PERCENTAGE));
        configuration.channels().add(new Channel("Blue", ChannelType.PERCENTAGE));
        configuration.channels().add(new Channel("Fog", ChannelType.BOOLEAN));
        channels.addAll(configuration.channels());

        setTitle(TITLE);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(400, 320));

        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Channels", channels);
        tabbedPane.setMnemonicAt(0, KeyEvent.VK_C);
        tabbedPane.addTab("Fixtures", buildFixtureComponent());
        tabbedPane.setMnemonicAt(1, KeyEvent.VK_I);
        tabbedPane.addTab("Devices", buildDeviceComponent());
        tabbedPane.setMnemonicAt(2, KeyEvent.VK_D);
        tabbedPane.addTab("Scenes", buildSceneComponent());
        tabbedPane.setMnemonicAt(3, KeyEvent.VK_S);
        tabbedPane.addTab("Shows", buildShowsComponent());
        tabbedPane.setMnemonicAt(4, KeyEvent.VK_H);
        setContentPane(tabbedPane);

        setVisible(true);
    }

    private static JMenuBar buildMenuBar(LightingControlConsole app) {
        var actionOpen = new JMenuItem("Open", KeyEvent.VK_O);
        actionOpen.getAccessibleContext().setAccessibleDescription("Search for a show file an load it to the Program");
        actionOpen.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileFilter(new LightingControlShowFileFilter());
            if (fileChooser.showOpenDialog(app) == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                // load from file
                LOGGER.info("Loading {}", file);
            }
        });
        var actionSave = new JMenuItem("Save", KeyEvent.VK_S);
        actionSave.getAccessibleContext().setAccessibleDescription("Store the current show to a show file");
        actionSave.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileFilter(new LightingControlShowFileFilter());
            if (fileChooser.showSaveDialog(app) == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                // load from file
                LOGGER.info("Saving {}", file);
            }
        });
        var actionQuit = new JMenuItem("Quit", KeyEvent.VK_Q);
        actionQuit.getAccessibleContext().setAccessibleDescription("Stop all processing and shutdown the Program");
        actionQuit.addActionListener(e -> app.dispose());
        var fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);
        fileMenu.add(actionOpen);
        fileMenu.add(actionSave);
        fileMenu.addSeparator();
        fileMenu.add(actionQuit);
        var bar = new JMenuBar();
        bar.add(fileMenu);
        return bar;
    }

    private Component buildShowsComponent() {
        return makeTextPanel("Shows");
    }

    private Component buildSceneComponent() {
        return makeTextPanel("Scenes");
    }

    private Component buildFixtureComponent() {
        return makeTextPanel("Fixtures");
    }

    private Component buildDeviceComponent() {
        return makeTextPanel("Device");
    }

    /**
     * Generate a simple text panel as long as not all panels are implemented.
     *
     * @param text Name of the panel to be configured.
     * @return Panel with just the given name dsplayed.
     * @deprecated Will be removed, once all panels are implemented.
     */
    @Deprecated(since = "1.0", forRemoval = true)
    public static Component makeTextPanel(String text) {
        JPanel panel = new JPanel(false);
        JLabel filler = new JLabel(text);
        filler.setHorizontalAlignment(SwingConstants.CENTER);
        panel.setLayout(new GridLayout(1, 1));
        panel.add(filler);
        return panel;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(LightingControlConsole::new);
    }

}
