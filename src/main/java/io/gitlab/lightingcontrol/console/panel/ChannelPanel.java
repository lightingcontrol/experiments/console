package io.gitlab.lightingcontrol.console.panel;

import io.gitlab.lightingcontrol.console.configuration.Channel;
import io.gitlab.lightingcontrol.console.configuration.ChannelType;
import io.gitlab.lightingcontrol.console.util.InEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Collection;

public class ChannelPanel extends JPanel {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChannelPanel.class);
    public static final Insets DEFAULT_INSETS = new Insets(3, 3, 3, 3);
    private final DefaultListModel<Channel> modelList;
    private final JList<Channel> viewList;
    private final InEditor<Channel> editorObject;
    private final JButton deleteButton;
    private final JSplitPane splitPane;
    private final JScrollPane editPanel;
    private final Component editInfo;
    private JTextField nameField;
    private JComboBox<ChannelType> typeField;

    public ChannelPanel() {
        setLayout(new BorderLayout());

        modelList = new DefaultListModel<>();
        viewList = new JList<>();
        editorObject = new InEditor<>();

        editPanel = makeEditPanel();
        editInfo = makeTextPanel("Choose channel");
        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, viewList, editInfo);

        viewList.setMinimumSize(new Dimension(120, 10));
        viewList.setDragEnabled(true);
        viewList.setModel(modelList);
        viewList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        deleteButton = new JButton("Delete");
        deleteButton.setEnabled(false);
        JButton addButton = new JButton("Add");

        var toolbar = new JToolBar();
        toolbar.setLayout(new GridLayout(1, 2));
        toolbar.add(addButton);
        toolbar.add(deleteButton);
        toolbar.setFloatable(false);
        add(toolbar, BorderLayout.NORTH);

        addButton.addActionListener(e -> {
            Channel newChannel = new Channel("New", ChannelType.PERCENTAGE);
            setEditor(newChannel, true);
            viewList.clearSelection();
        });
        deleteButton.addActionListener(e -> {
            Channel selectedValue = viewList.getSelectedValue();
            LOGGER.debug("Deleting: {}", selectedValue);
            modelList.removeElement(selectedValue);
            deleteButton.setEnabled(false);
            splitPane.setRightComponent(editInfo);
        });

        viewList.addListSelectionListener(e -> {
            Channel selectedValue = viewList.getSelectedValue();
            if (selectedValue != null) {
                setEditor(selectedValue, false);
                deleteButton.setEnabled(true);
                LOGGER.debug("Selected: {}", viewList.getSelectedValue());
            }
        });

        add(splitPane, BorderLayout.CENTER);
    }

    private JScrollPane makeEditPanel() {
        var panel = new JPanel();
        var layout = new GridBagLayout();
        panel.setLayout(layout);

        var nameLabel = new JLabel("Name");
        panel.add(nameLabel, toColumn(0, 0));

        nameField = new JTextField();
        panel.add(nameField, toColumn(1, 0, GridBagConstraints.WEST));

        var typeLabel = new JLabel("Type");
        panel.add(typeLabel, toColumn(0, 1));

        typeField = new JComboBox<>();
        typeField.setModel(new DefaultComboBoxModel<>(ChannelType.values()));
        panel.add(typeField, toColumn(1, 1, GridBagConstraints.WEST));

        JButton saveButton = new JButton("Save");
        saveButton.addActionListener(this::save);
        panel.add(saveButton, toColumn(1, 2));

        return new JScrollPane(panel);
    }

    private void save(ActionEvent actionEvent) {
        var channel = editorObject.getValue();
        if(channel != null) {
            channel.setName(nameField.getText());
            channel.setType((ChannelType) typeField.getSelectedItem());
            if (editorObject.isNew()) {
                this.modelList.addElement(editorObject.getValue());
                this.viewList.setSelectedValue(editorObject.getValue(), true);
            }
            this.viewList.updateUI();
        }
    }

    private GridBagConstraints toColumn(int x, int y) {
        return toColumn(x, y, GridBagConstraints.CENTER);
    }

    private GridBagConstraints toColumn(int x, int y, int anchor) {
        var constraints = new GridBagConstraints();
        constraints.gridx = x;
        constraints.gridy = y;
        constraints.anchor = anchor;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.insets = DEFAULT_INSETS;
        return constraints;
    }

    private void setEditor(Channel channel, boolean isNew) {
        editorObject.setValue(channel, isNew);
        nameField.setText(channel.getName());
        typeField.setSelectedItem(channel.getType());
        if (isNew) {
            viewList.clearSelection();
            deleteButton.setEnabled(false);
        } else {
            viewList.setSelectedValue(channel, true);
        }
        splitPane.setRightComponent(editPanel);
    }

    public void addAll(Collection<Channel> channels) {
        modelList.addAll(channels);
    }

    public static Component makeTextPanel(String text) {
        JPanel panel = new JPanel(false);
        JLabel filler = new JLabel(text);
        filler.setHorizontalAlignment(SwingConstants.CENTER);
        panel.setLayout(new GridLayout(1, 1));
        panel.add(filler);
        return panel;
    }
}
