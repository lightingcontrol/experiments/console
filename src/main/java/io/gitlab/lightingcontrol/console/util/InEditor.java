package io.gitlab.lightingcontrol.console.util;

import javax.annotation.processing.Generated;
import java.io.Serializable;

/**
 * {@link InEditor} represents a value which is loaded to be modified. To keep track of changes, this class keeps the
 * additional information.
 *
 * @param <C> The class opened to edit.
 */
public class InEditor<C extends Serializable> implements Serializable {

    private C value;
    private boolean newObject;

    public InEditor() {
    }

    public InEditor(C value, boolean isNew) {
        this.value = value;
        this.newObject = isNew;
    }

    @Generated("getter")
    public C getValue() {
        return value;
    }

    @Generated("setter")
    public void setValue(C value) {
        this.value = value;
    }

    @Generated("setter")
    public void setValue(C value, boolean isNew) {
        this.value = value;
        this.newObject = isNew;
    }

    @Generated("getter")
    public boolean isNew() {
        return newObject;
    }

    @Generated("setter")
    public void setNew(boolean newObject) {
        this.newObject = newObject;
    }

    @Override
    public String toString() {
        return "%s (%s)".formatted(value, newObject ? "new" : "persisted");
    }
}
