package io.gitlab.lightingcontrol.console.util;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class LightingControlShowFileFilter extends FileFilter {
    @Override
    public String getDescription() {
        return "LighingColtrol Show (*.lcs)";
    }

    @Override
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        } else {
            String filename = f.getName().toLowerCase();
            return filename.endsWith(".lcs");
        }
    }
}
