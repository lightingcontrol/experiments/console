package io.gitlab.lightingcontrol.console.configuration;

import java.io.Serial;
import java.io.Serializable;
import java.util.Vector;

public record Configuration(Vector<Channel> channels) implements Serializable {

    @Serial
    private static final long serialVersionUID = 8953606190559864005L;

}
