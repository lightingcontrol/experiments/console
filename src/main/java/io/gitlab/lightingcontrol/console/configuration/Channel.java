package io.gitlab.lightingcontrol.console.configuration;

import javax.annotation.processing.Generated;
import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class Channel implements Serializable {

    @Serial
    private static final long serialVersionUID = 313563524328974888L;

    private UUID id;
    private String name;
    private ChannelType type;

    public Channel() {
        this(null, null);
    }

    public Channel(String name, ChannelType type) {
        this(UUID.randomUUID(), name, type);
    }

    public Channel(UUID id, String name, ChannelType type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    @Generated("getter")
    public UUID getId() {
        return id;
    }

    @Generated("setter")
    public void setId(UUID id) {
        this.id = id;
    }

    @Generated("getter")
    public String getName() {
        return name;
    }

    @Generated("setter")
    public void setName(String name) {
        this.name = name;
    }

    @Generated("getter")
    public ChannelType getType() {
        return type;
    }

    @Generated("setter")
    public void setType(ChannelType type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, type);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof Channel other))
            return false;
        return Objects.equals(this.id, other.id) &&
                Objects.equals(this.name, other.name) &&
                Objects.equals(this.type, other.type);
    }

    @Override
    public String toString() {
        return name + " (" + (type == null ? '?' : type.getShorty()) + ")";
    }
}
