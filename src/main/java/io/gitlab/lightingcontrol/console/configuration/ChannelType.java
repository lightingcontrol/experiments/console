package io.gitlab.lightingcontrol.console.configuration;

import javax.annotation.processing.Generated;
import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;

public enum ChannelType implements Serializable {
    PERCENTAGE("Percent", BigDecimal.ZERO, BigDecimal.valueOf(100), 4, '%'),
    BOOLEAN("Boolean", BigDecimal.ZERO, BigDecimal.ONE, 0, '⇄');

    @Serial
    private static final long serialVersionUID = -5132184256055881800L;

    private final String name;
    private final BigDecimal from;
    private final BigDecimal to;
    private final int scale;
    private final char shorty;

    ChannelType(String name, BigDecimal from, BigDecimal to, int scale, char shorty) {
        this.name = name;
        this.from = from;
        this.to = to;
        this.scale = scale;
        this.shorty = shorty;
    }

    @Generated("getter")
    public String getName() {
        return name;
    }

    @Generated("getter")
    public BigDecimal getFrom() {
        return from;
    }

    @Generated("getter")
    public BigDecimal getTo() {
        return to;
    }

    @Generated("getter")
    public int getScale() {
        return scale;
    }

    @Generated("getter")
    public char getShorty() {
        return shorty;
    }

    @Override
    public String toString() {
        return String.format("%s (%s-%s)", name, from, to);
    }
}
