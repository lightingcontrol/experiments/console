package io.gitlab.lightingcontrol.console.configuration;

import io.gitlab.lightingcontrol.console.util.InEditor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class ChannelTypeTest {

    public static Stream<Arguments> shouldProvideSpecifiedParameters() {
        return Stream.of(
                Arguments.of(ChannelType.PERCENTAGE, "Percent", 4),
                Arguments.of(ChannelType.BOOLEAN, "Boolean", 0)
        );
    }

    @MethodSource
    @ParameterizedTest
    void shouldProvideSpecifiedParameters(ChannelType sut, String expectedName, int expectedScale) {
        assertThat(sut.getName()).isEqualTo(expectedName);
        assertThat(sut.getScale()).isEqualTo(expectedScale);
    }

    @Test
    void shouldByDefaultNotBeNewValue() {
        var sut = new InEditor<>(); // System Under Test

        assertThat(sut).hasFieldOrPropertyWithValue("new", false);
    }

    @Test
    void shouldCheckForAllChannelTypes() {
        long parameterizedTests = ChannelTypeTest.shouldProvideSpecifiedParameters().count();
        int actualChannelTypes = ChannelType.values().length;

        assertThat(parameterizedTests)
                .withFailMessage("parameters for ChannelTypeTest does not check on the number of actually available ChannelTypes. There are %d types but %d tested ones.", actualChannelTypes, parameterizedTests)
                .isEqualTo(actualChannelTypes);
    }

}