package io.gitlab.lightingcontrol.console.configuration;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;

class ChannelTest {

    public static Stream<Arguments> shouldRenderToStringWithNameAndType() {
        return Stream.of(
                Arguments.of(new Channel(), "null (?)"),
                Arguments.of(new Channel("Test", ChannelType.PERCENTAGE), "Test (%)"),
                Arguments.of(new Channel("Channel", ChannelType.BOOLEAN), "Channel (⇄)")
        );
    }

    @ParameterizedTest
    @MethodSource
    void shouldRenderToStringWithNameAndType(Channel sut, String expectedRepresentation) {
        assertThat(sut).hasToString(expectedRepresentation);
    }

    @Test
    void shouldSetRandomIdOnCreation() {
        var sut = new Channel(); // System Under Test

        assertThat(sut).hasAllNullFieldsOrPropertiesExcept("id");
        assertThat(sut.getId()).isNotEqualTo(new Channel().getId());
    }

    @Test
    void shouldAllowChangingTheId() {
        var sut = new Channel(); // System Under Test

        var initialId = sut.getId();
        sut.setId(randomUUID());

        assertThat(sut.getId()).isNotEqualTo(initialId);
    }

    @Test
    void shouldEqualOtherChannelWithTheSameParameters() {
        var sut = new Channel(); // System Under Test
        var other = new Channel();
        other.setId(sut.getId());

        assertThat(sut).isEqualTo(other);
    }

    @Test
    void shouldEqualHashCoddOfOtherChannelWithTheSameParameters() {
        var sut = new Channel(); // System Under Test
        var other = new Channel();
        other.setId(sut.getId());

        assertThat(sut).hasSameHashCodeAs(other);
    }

}