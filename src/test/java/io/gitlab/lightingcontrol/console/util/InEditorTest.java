package io.gitlab.lightingcontrol.console.util;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

class InEditorTest {

    public static Stream<Arguments> shouldRenderToString() {
        return Stream.of(
                Arguments.of(new InEditor<>("Hello", true), "Hello (new)"),
                Arguments.of(new InEditor<>("Hello", false), "Hello (persisted)"),
                Arguments.of(new InEditor<>(123, true), "123 (new)"),
                Arguments.of(new InEditor<>(123, false), "123 (persisted)")
        );
    }

    @MethodSource
    @ParameterizedTest
    void shouldRenderToString(InEditor<?> sut, String expectedResult) {
        Assertions.assertThat(sut).hasToString(expectedResult);
    }

}